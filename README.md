# Mathezirkel Website

[![Hugo](https://img.shields.io/badge/Hugo-black.svg?style=for-the-badge&logo=Hugo)](https://gohugo.io/)
[![Nginx](https://img.shields.io/badge/nginx-%23009639.svg?style=for-the-badge&logo=nginx&logoColor=white)](https://www.nginx.com/)
[![Django](https://img.shields.io/badge/django-%23092E20.svg?style=for-the-badge&logo=django&logoColor=white)](https://www.djangoproject.com)

[![linting: pylint](https://img.shields.io/badge/linting-pylint-yellowgreen)](https://github.com/pylint-dev/pylint)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)

Repository containing sources of the website at https://www.mathezirkel-augsburg.de/.

## Contribution

1. Create a new branch from `staging` for your work, and name it accordingly.
2. Submit a merge request for your branch.

### Adding and editing pages

All pages of the website are stored as Markdown files within the [content directory](./frontend/content/).

## Testing

### Static files only

If you only want to test the static files version of the website using Hugo, you can use the following Docker command:

```commandline
docker run --rm -it \
-v $(pwd)/frontend:/src \
-p 1313:1313 \
--entrypoint "/usr/bin/hugo" \
registry.gitlab.com/pages/hugo/hugo_extended:0.118.2 \
server \
--bind=0.0.0.0 \
--port=1313
```

This will start a container running the Hugo web server and serving the static files created by Hugo. The web server is available at http://localhost:1313 in your browser.

### Full Stack

To run the full stack version of the website locally, use the [test script](./test/test). You can start the containers by running the following command in your terminal:

```commandline
/bin/sh ./test/test.sh
```

Make sure to link the correct environment files.

Once the containers are up and running, you can access the website by going to http://localhost:8080 in your web browser.

**Note**: Please note that the credentials for email support do not need to be valid for testing purposes.

### Credentials

If you require any credentials, please reach out to the organization team for assistance.

## Deployment

This section outlines the deployment process for our project, with two key environments: Staging and Production.

### Deployment on staging

Any commit on the staging branch will initiate the automated build process. The container image will be tagged with the shortened commit SHA and pushed to the registry. These images are built based on the committed source code. The images are also tagged `staging`.

The newly created container images will be automatically deployed to our staging environment at `staging.mathezirkel-augsburg.de`. This ensures that the latest versions of frontend and backend are readily available for testing and validation.

### Deployment on production

Merging the staging branch into the main branch will also initiate an automated build process as for staging.
Unlike above the images will be tagged `main` and subsequently deployed to our production environment at `www.mathezirkel-augsburg.de`.

## Documentation

### Frontend

The Frontend container operates an NGINX server designed to serve static files, which are built by Hugo. Configuration of this server can be done through the [NGINX Config Template](./nginx/default.conf.template) and the environment variable listed below:

* `BACKEND_SERVICE`

    This is the name of the service through which the backend is accessible.

### Backend

The backend container runs a Django application, which can be configured utilizing environment variables. A sample configuration can be found at the following location:
[Sample Configuration](./test/cabret.example.env).

The following table illustrates the configurable environment variables, aligning with the built-in settings of Django. Please refer to the [Django documentation](https://docs.djangoproject.com/en/4.2/ref/settings/) for a more detailed explanation of each setting.

| Variable | Django setting | Default |
|----------|-------------|---------|
| `DJANGO_SECRET_KEY` | `SECRET_KEY` | `django-insecure` |
| `DJANGO_DEBUG` | `DEBUG` | `0` |
| `DJANGO_ALLOWED_HOSTS` | `ALLOWED_HOSTS` | `localhost` |
| `DJANGO_ALLOWED_CIDR_NETS`| `ALLOWED_CIDR_NETS` | `127.0.0.1` |
| `DJANGO_CSRF_TRUSTED_ORIGINS` | `CSRF_TRUSTED_ORIGINS` | `http://localhost` |
| `DJANGO_DEFAULT_FROM_EMAIL` | `DEFAULT_FROM_EMAIL` | `""` |
| `DJANGO_EMAIL_HOST` | `EMAIL_HOST` | `""` |
| `DJANGO_EMAIL_PORT` | `EMAIL_PORT` | `25` |
| `DJANGO_EMAIL_HOST_USER` | `EMAIL_HOST_USER` | `""` |
| `DJANGO_EMAIL_HOST_PASSWORD` | `EMAIL_HOST_PASSWORD` | `""` |
| `DJANGO_EMAIL_USE_TLS` | `EMAIL_USE_TLS` | `0` |
| `DJANGO_EMAIL_USE_SSL` | `EMAIL_USE_SSL` | `0` |

Furthermore, there are the following settings:

* `DJANGO_PRODUCTION`

    When the variable `DJANGO_PRODUCTION` is enabled, the application transitions into production mode and automatically adjusts several settings to enhance security, making the assumption that the application operates behind an SSL terminating proxy. Below is a table that outlines the values assigned to each setting in production mode.
    | Django setting | Value |
    |----------------|-------|
    | `SECURE_PROXY_SSL_HEADER` | `('HTTP_X_FORWARDED_PROTO', 'https')` |
    | `USE_X_FORWARDED_HOST` | `True` |
    | `SECURE_CONTENT_TYPE_NOSNIFF` | `True` |
    | `SECURE_CROSS_ORIGIN_OPENER_POLICY` | `same-origin` |
    | `SECURE_REFERRER_POLICY` | `same-origin` |
    | `X_FRAME_OPTIONS` | `DENY` |
    | `SESSION_COOKIE_SECURE` | `True` |

    Default: `1`

* `DJANGO_RATELIMITS`

    The `DJANGO_RATELIMITS` variable allows the configuration of rate limits applicable to POST requests. The variable expects a space-separated list of rate limits, more information on the syntax can be found [here](https://django-ratelimit.readthedocs.io/en/stable/usage.html).
    Default: `5/d`

* Redis

    | Variable | Default | Description |
    |----------|---------|-------------|
    | `REDIS_HOST` | `redis` | Hostname of the redis service. |
    | `REDIS_PORT` | `6379` | Port of the redis service. |
    | `REDIS_PASSWORD` | `""` | Password of the redis service. |

## Acknowledgments

This website was built using the following frameworks and third-party code:

- [Hugo](https://gohugo.io/) - The static site generator used to build this website.
- [NGINX](https://www.nginx.com) - A free, open-source, high-performance HTTP server to serve the static files.
- [Python Django](https://www.djangoproject.com/) - The web framework used to develop the backend of this website.
- The icon set used in this website's design:
  - [Feather Icons](https://feathericons.com/)
  - [Atlas icons](https://atlasicons.vectopus.com/)

We appreciate the contributions of these tools and resources to our website's development.

## License

The repository is mainly licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt), see [LICENSE.md](./LICENSE.md) for details.
