window.onload = function toggleNav() {
    const nav = document.getElementById("nav");
    const hamburger = document.getElementById("hamburger");

    hamburger.addEventListener('click', () => {nav.classList.toggle("nav-dropped")});
}

