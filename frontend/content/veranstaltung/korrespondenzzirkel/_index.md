---
title: Korrespondenzzirkel
type: event
image: /images/zirkelgregor.png
registration: true
registrationLink: /anmeldung/anmeldung-zirkel.pdf
feedback: True
donate: False
summary: |
    Wir senden dir alle vier bis sechs Wochen per Post oder E-Mail einen Brief zu. Darin findest du eine Einführung in ein mathematisches Thema mit ausführlichen Erklärungen, Beispielen und Aufgaben. Die Aufgaben kannst du freiwillig bearbeiten. Wenn du uns deine Lösungen zusendest, korrigieren wir diese gerne für dich.
---

Wenn du zu weit entfernt wohnst, um regelmäßig nach Augsburg zu kommen, oder einfach nicht die Zeit hast, an den Präsenzzirkeln teilzunehmen, kannst du dennoch am Korrespondenzzirkel teilnehmen.

![Bild zur Veranstaltung](/static/veranstaltung/korrespondenz.jpg)

Wir senden dir alle vier bis sechs Wochen per Post oder E-Mail einen Brief zu. Darin findest du eine Einführung in ein mathematisches Thema mit ausführlichen Erklärungen, Beispielen und Aufgaben. Die Aufgaben kannst du freiwillig bearbeiten. Wenn du uns deine Lösungen zurücksendet, korrigieren wir diese gerne für dich.

Die Themen sind an die entsprechenden Klassenstufen angepasst und enthalten sowohl einfache als auch anspruchsvolle Aufgaben, sodass für jede:n etwas dabei ist.

Mögliche Themen sind:

* Logikrätsel
* Magische Quadrate
* Graphentheorie
* Verschlüsselung
* Relativitätstheorie

Wenn du dich anmelden möchtest, klicke einfach rechts auf anmelden und sende uns das ausgefüllte und unterschriebene Anmeldeformular per E-Mail oder Post zu. Die Teilnahme ist kostenlos.

