---
title: Auftaktveranstaltung
type: event
image: /images/gregor-schokolade.png
registration: true
registrationLinkText: Zirkelanmeldung
registrationLink: /anmeldung/anmeldung-zirkel.pdf
feedback: False
donate: True
---

Um euch einen Einblick in den Mathezirkel zu geben, findet am Samstag, den 21.10.2023, um 10 Uhr unsere Eröffnungsveranstaltung im Hörsaal 1004 im Gebäude T der Uni Augsburg statt.
Eine Anmeldung dafür ist nicht erforderlich.

![Bild zur Veranstaltung](/static/veranstaltung/auftakt.jpg)

Als Highlight steht ein allgemeinverständlicher mathematischer Vortrag auf dem Programm:

{{% div class="centering margin-bottom" %}}
Prof. Dr. André Uschmajew

Unendliche Zufallsfolgen -- Möglich ist alles!
{{% /div %}}

Nach dem Vortrag haben wir noch ein wenig Zeit eingeplant, um ins Gespräch zu kommen.
Bei Brezen und Getränken auf Spendenbasis habt ihr die Möglichkeit, schon mal andere Zirkelteilnehmer:innen und -leiter:innen kennen zu lernen.

### Auftakträtsel

Louisa möchte eine Tafel Schokolade für ihre Freund:innen auf dem Mathecamp in lauter Einzelstücke brechen.
Die Tafel besteht aus 4x6 Stücken.
Wie oft muss sie dazu die Tafel entlang der Rillen mindestens brechen?

Hat dir das Rätsel gefallen? Dann kannst du dich rechts für die Präsenz- und Korrespondenzzirkel anmelden! Die Auflösung gibt es dann bei der Auftaktveranstaltung.
