---
title: Astrotag
type: event
image: /images/zirkelgregor.png
registration: false
registrationLink: /anmeldung/anmeldung-astrotag.pdf
feedback: True
donate: False
summary: Im Januar dreht sich alles rund um die Themen Astronomie, Astrophysik und Raumfahrt.

---

Am Astrotag dreht sich alles rund um die Themen Astronomie, Astrophysik und Raumfahrt.

Vormittags stehen spannende Vorführungen und Experimente auf dem Programm. In den letzten Jahren waren wir beispielsweise im [S-Planetarium Augsburg](https://www.s-planetarium.de/) oder im [DLR_School_Lab](https://www.uni-augsburg.de/de/forschung/einrichtungen/institute/amu/bildung/school_lab/). Sei also gespannt, was dich 2024 erwartet!

Nachmittags bieten wir ein buntes Freizeitprogramm an, dazu gehören u. a. Programmierkurse (für jedes Niveau), eine Campus-Rallye sowie weitere mathematische Seminare.

Die Mathetage bieten insbesondere denen eine großartige Möglichkeit, die aufgrund ihres Wohnorts nicht regelmäßig an den Präsenzzirkeln teilnehmen können. Außerdem bekommt ihr einen kleinen Einblick in das, was euch auf dem Mathecamp erwartet.

### Termine

Der nächste Astrotag findet im Januar 2024 statt. Weitere Infos folgen!
