---
title: Wintercamp
type: event
image: /images/zirkelgregor.png
registration: false
registrationLink: /anmeldung/anmeldung-camp.pdf
feedback: True
donate: False
summary: |
    Seit 2023 gibt es auch in den Winterferien ein Mathecamp.
    Das Camp ist im Vergleich zum Sommercamp deutlich kleiner.
    Die gemütliche und winterliche Atmosphäre bietet einen hervoragende Möglichkeit für einen intensiven mathematischen Austausch.
---

Seit 2023 gibt es auch in den Winterferien ein Mathecamp. Das Camp ist im Vergleich zum Sommercamp deutlich kleiner. Die gemütliche und winterliche Atmosphäre bietet einen hervorragende Möglichkeit für einen intensiven mathematischen Austausch.

![Bild zur Veranstaltung](/static/veranstaltung/wintercamp.jpg)

Vormittags finden mathematische Seminare statt, in denen du in Kleingruppen spannende Bereiche der Mathematik kennenlernst. Selbstverständlich gibt es für verschiedene Alters- und Vorkenntnisstufen angepasste Kurse mit unterschiedlichen Themen und Schwierigkeiten, sodass immer für jede:n etwas dabei ist. Zusätzlich gibt es nachmittags weitere, optionale Arbeitsgruppen.

Willst du einen Schneemann bauen? Auch wenn der Fokus des Camps stärker auf der Mathematik liegt, bietet das Haus mit seiner Turnhalle, der Kegelbahn und den Werkräumen auch im Winter ausreichend Möglichkeit für ein buntes Freizeitprogramm. Dieses besteht u. a. aus Programmierworkshops, Mal- und Nähstunden, einer Nachtwanderung sowie aus Spielen und den verschiedensten Sportarten. Darüber hinaus werden wir bei schönem Wetter auch viel im Freien machen können.

Das Wintercamp verspricht eine unvergessliche Zeit voller Mathematik und Spaß!

### Termin 2024

Das nächste Wintercamp findet vom 10. bis 16. Februar 2024 statt. Weitere Infos folgen!


### Vergangene Camps

* [Wintercamp 2023](./2023/)
