---
title: Mai-Mathetag
type: event
image: /images/zirkelgregor.png
registration: false
registrationLink: /anmeldung/anmeldung-astrotag.pdf
feedback: True
donate: False
summary: |
    Am Mai-Mathetag hast du die Gelegenheit, gemeinsam mit anderen mathematikbegeisterten Schüler:innen einen Tag voller Mathematik und Spaß zu verbringen.
---

Am Mai-Mathetag hast du die Gelegenheit, gemeinsam mit anderen mathematikbegeisterten Schüler:innen einen Tag voller Mathematik und Spaß zu verbringen.

![Bild zur Veranstaltung](/static/veranstaltung/mai-mathetag.jpg)

Vormittags finden mathematische Seminare in Kleingruppen statt, in denen du gemeinsam mit ehrenamtlichen Tutor:innen spannende mathematische Themen interaktiv entdeckst. Nachmittags gibt es neben einem kleinen mathematischen Wettkampf ein buntes Freizeitangebot, welches u. a. Programmierkurse (für jedes Niveau), eine Campus-Rallye sowie weitere mathematische Seminare, beinhaltet.

Die Mathetage bieten insbesondere denen eine großartige Möglichkeit, die aufgrund ihres Wohnorts nicht regelmäßig an den Präsenzzirkeln teilnehmen können. Außerdem bekommt ihr einen kleinen Einblick in das, was euch auf dem Mathecamp erwartet.

### Termine

Der nächste Mai-Mathetag findet im Mai 2024 statt. Weitere Infos folgen!
