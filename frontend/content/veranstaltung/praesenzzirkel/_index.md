---
title: Präsenzzirkel
type: event
image: /images/zirkelgregor.png
registration: true
registrationLink: /anmeldung/anmeldung-zirkel.pdf
feedback: True
donate: False
summary: |
    Du hast Spaß an Mathematik? Unsere Präsenzzirkel bieten eine hervorragende Gelegenheit, weiterführende faszinierende Themen der Mathematik zu entdecken.
    Dazu triffst du dich regelmäßig mit anderen Teilnehmer:innen und ehrenamtlichen Tutor:innen an der Uni Augsburg.
    Gemeinsam lernt ihr in kleinen Gruppen interaktiv spannende mathematische Konzepte kennen und bearbeitet knifflige Aufgaben.
---

Du hast Spaß an Mathematik? Unsere Präsenzzirkel bieten eine hervorragende Gelegenheit, weiterführende faszinierende Themen der Mathematik zu entdecken. Dazu triffst du dich regelmäßig mit anderen Teilnehmer:innen und ehrenamtlichen Tutor:innen an der Uni Augsburg. Gemeinsam lernt ihr in kleinen Gruppen interaktiv spannende mathematische Konzepte kennen und bearbeitet knifflige Aufgaben.

![Bild zur Veranstaltung](/static/veranstaltung/praesenz.jpg)

Die Präsenzzirkel sind nicht als Nachhilfe oder Wiederholung von Schulstoff gedacht, sondern behandeln weiterführende Inhalte außerhalb des Schulstoffs.

Mögliche Themen sind:

* Logikrätsel
* Magische Quadrate
* Graphentheorie
* Verschlüsselung
* Relativitätstheorie

Selbstverständlich sind die Inhalte an die Klassenstufen angepasst. Bei der Auswahl der Inhalte orientieren wir uns natürlich an deinen Interessen und Bedürfnissen. Unsere Themen und Fragestellungen sind definitiv anders als das, was du aus der Schule kennst.

Wenn du dich anmelden möchtest, klicke einfach rechts auf anmelden und sende uns das ausgefüllte und unterschriebene Anmeldeformular per E-Mail oder Post zu. Die Teilnahme ist kostenlos.

### Termine

Unsere Präsenzzirkel dauern 90 Minuten und finden in einem 2-Wochen-Rhythmus statt. Die konkreten Termine werden nach der Auftaktveranstaltung anhand eurer Terminpräferenzen festgelegt.
