---
title: Mathecamp
type: event
image: /images/zirkelgregor.png
registration: false
registrationLink: /anmeldung/anmeldung-camp.pdf
feedback: True
donate: False
summary: |
    Mitten in den Sommerferien habt ihr die Gelegenheit, neun Tage mit uns im Bruder-Klaus-Heim in Violau zu verbringen. Wir werden gemeinsam vielfätige mathematische Themen entdecken und verstehen. Dabei kommt die Freizeit aber auf keinen Fall zu kurz. Außerdem ergeben sich hier viele Möglichkeiten, Gleichgesinnte kennenzulernen und neue Freundschaften zu knüpfen.
---

Mitten in den Sommerferien habt ihr die Gelegenheit, neun Tage mit uns im Bruder-Klaus-Heim in Violau zu verbringen. Wir werden gemeinsam vielfältige mathematische Themen entdecken und verstehen. Dabei kommt die Freizeit aber auf keinen Fall zu kurz. Außerdem ergeben sich hier viele Möglichkeiten, Gleichgesinnte kennenzulernen und neue Freundschaften zu knüpfen.

![Bild zur Veranstaltung](/static/veranstaltung/mathecamp.jpg)

Vormittags finden mathematische Seminare statt, in denen du in Kleingruppen spannende Bereiche der Mathematik kennenlernst. Selbstverständlich gibt es für verschiedene Alters- und Vorkenntnisstufen angepasste Kurse mit unterschiedlichen Themen und Schwierigkeiten, sodass immer für jede:n etwas dabei ist. Zusätzlich gibt es nachmittags weitere, optionale Arbeitsgruppen.

Daneben bieten wir ein buntes Freizeitprogramm an. Dieses besteht u. a. aus Programmierworkshops, Mal- und Nähstunden, einer Nachtwanderung, einem Chor und einem Orchester, einem Tanzkurs sowie aus Spielen und den verschiedensten Sportarten. Bei schönem Wetter werden wir auch viel im Freien machen können. Das Haus bietet einen Fußballplatz, ein Beachvolleyball-Feld, einen See zum Bootfahren, einen Pool und vieles mehr!

Das Mathecamp verspricht eine unvergessliche Zeit voller Mathematik und Spaß!


### Termin 2024

Das nächste Mathecamp findet vom 17. bis 25. August 2024 statt. Weitere Infos folgen!

### Vergangene Camps

* [Mathecamp 2022](./2022/)
* [Mathecamp 2023](./2023/)
