---
title: Ankündigungen
_build:
  render: never
  list: never
---

#### Auftaktveranstaltung

am 21.10.2023 um 10 Uhr
im Gebäude T der Universität Augsburg\
[Mehr](/veranstaltung/auftaktveranstaltung)


#### Präsenz- und Korrespondenzzirkel

Die Anmeldephase ist eröffnet.
