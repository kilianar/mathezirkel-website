---
title: "Unterstützung und Kontakt"
layout: "single"
---

{{% div class="margin-bottom" %}}
Die Teilnahme an den Präsenz- und Korrespondenzzirkel des Mathezirkels ist für Schüler:innen kostenlos. Um die dennoch anfallenden Kosten für Dinge wie Materialien, Bücher und Briefe zu decken, sind wir daher auf Spenden angewiesen. Wir würden uns freuen, wenn Sie einen Beitrag zur Fortführung des Mathezirkels leisten wollen! Der Mathematisch-Physikalische Verein ist gemeinnützig, so dass Spenden von der Steuer abgesetzt werden können. Für Spenden bis 200 Euro genügt dafür der Kontoauszug, wir schicken Ihnen aber natürlich dennoch gerne eine Spendenbescheinigung zu.

**Kontoinhaber**: Mathematisch-Physikalischer Verein e. V.

**IBAN**: DE97 7205 0000 0251 0746 96

**BIC**: AUGSDE77XXX
{{% /div %}}

{{% div class="margin-bottom" %}}
Für Rückfragen und zusätzliche Informationen wenden Sie sich bitte an die folgenden Kontaktdaten oder verwenden unser Kontaktformular.

**E-Mail**: [mathezirkel@math.uni-augsburg.de](mailto:mathezirkel@math.uni-augsburg)

**Telefon**: 0821 598 5806
{{% /div %}}

{{<
    contact-form
    checkbox="Ich stimme der Datenverarbeitung gemäß der [Datenschutzerklärung](/datenschutz) zu."
>}}
