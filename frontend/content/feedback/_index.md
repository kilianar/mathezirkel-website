---
title: "Feedback"
layout: "single"
---

{{<
    feedback-form
    checkbox="Ich stimme der Datenverarbeitung gemäß der [Datenschutzerklärung](/datenschutz) zu."
    note="Die Nachricht wird anonymisiert an dein:e Zirkelleiter:innen gesendet."
>}}
