---
title: Materialien
---

* [Cosmic Call -- Botschaften](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2016/cosmic-call/message.pdf)
* [Cosmic Call -- Auflösung](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2016/cosmic-call/spoiler.pdf)
* [Dodekalendar](https://mathezirkel.gitlab.io/content/dodekalendar/dodekalendar.pdf)
* [Experimentierkasten Verschlüsselung](./kryptographie/)
* [Anleitung für Python Turtle -- Grundlagen und einfache Fraktale](https://gitlab.com/mathezirkel-taulex/zirkel-python-turtle-fraktale/-/blob/main/tutorials/README.md)

