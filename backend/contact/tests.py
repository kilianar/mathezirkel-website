"""
Contact Form Test Module
=========================

This module is dedicated to the testing of the ContactForm functionality and its related views
in Django. It evaluates the responses to both valid and invalid form submissions and assesses
the rate-limiting mechanism implemented. Note that the captcha functionality is deliberately
excluded from this testing.

Classes:
--------
ContactFormTest : TestCase
    Class containing test cases for the ContactForm.

ContactFormViewTest : TestCase
    Class containing test cases for the view that renders the ContactForm.

Attributes:
-----------
VALID_DATA : dict
    A dictionary containing a set of valid data for the ContactForm.

INVALID_DATA_SETS : list
    A list of dictionaries, each containing a set of invalid data for the ContactForm to test.

"""

from unittest.mock import patch
from django.utils.html import escape
from django.urls import reverse
from django.test import TestCase, override_settings
from django.conf import settings
from .forms import ContactForm

VALID_DATA = {
    'email_field': 'test@example.com',
    'message_field': 'This is a test message.',
    'captcha_field': ['mocked_value', 'mocked_value'],
    'privacy_agreement_checkbox': True
}

INVALID_DATA_SETS = [
    {'email_field': ''},
    {'email_field': 'x'},
    {'email_field': 'x@'},
    {'email_field': '@x'},
    {'message_field': ''},
    {'message_field': 'x'},
    {'privacy_agreement_checkbox': 'False'},
]

class ContactFormTest(TestCase):
    """
    ContactForm Test Class

    This class contains test cases to test the functionality of the ContactForm.

    Methods:
    --------
    test_form_valid():
        Test case to verify that emails are sent correctly when valid data is submitted.

    test_form_invalid():
        Test case to verify form behavior when invalid data is submitted.

    test_html_escaping():
        Test case to verify that HTML is properly escaped.
    """


    @patch('contact.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('contact.forms.EmailMessage')
    def test_form_valid(self, mock_email_message, _mock_captcha_clean):
        """
        Test case to verify that emails are sent correctly when valid data is submitted.
        """
        mock_email_instance = mock_email_message.return_value
        form = ContactForm(data=VALID_DATA)
        if not form.is_valid():
            print(form.errors)
        self.assertTrue(form.is_valid())
        form.send_email()
        self.assertTrue(mock_email_instance.send.called)
        _, constructor_kwargs = mock_email_message.call_args
        self.assertEqual(constructor_kwargs['body'], 'This is a test message.')
        self.assertEqual(constructor_kwargs['reply_to'][0], VALID_DATA['email_field'])

    @patch('contact.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('contact.forms.EmailMessage')
    def test_form_invalid(self, mock_email_message, _mock_captcha_clean):
        """
        Test case to verify form behavior when invalid data is submitted.
        """
        for invalid_data in INVALID_DATA_SETS:
            with self.subTest(invalid_data=invalid_data):
                test_data = VALID_DATA.copy()
                test_data.update(invalid_data)
                form = ContactForm(data=test_data)
                self.assertFalse(form.is_valid())
                for field in invalid_data:
                    self.assertIn(field, form.errors)
                self.assertFalse(mock_email_message.called)


    @patch('contact.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('contact.forms.EmailMessage')
    def test_html_escaping(self, mock_email_message, _mock_captcha_clean):
        """
        Test case to verify that HTML is properly escaped.
        """
        dangerous_data = VALID_DATA.copy()
        dangerous_data['message_field'] = '<script>alert("hack");</script>'
        form = ContactForm(data=dangerous_data)
        self.assertTrue(form.is_valid())
        form.send_email()
        _, constructor_kwargs = mock_email_message.call_args
        escaped_message = escape(dangerous_data['message_field'])
        self.assertEqual(constructor_kwargs['body'], escaped_message)

@override_settings(
    CACHES={
        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
            'LOCATION': 'unique-snowflake',
        }
    }
)
class ContactFormViewTest(TestCase):
    """
    ContactFormView Test Class

    This class contains test cases to test the view that renders the ContactForm.

    Attributes:
    -----------
    url : str
        The URL to which the form will be posted.

    Methods:
    --------
    test_form_valid():
        Test case to verify that the form correctly redirects upon valid submission.

    test_form_invalid():
        Test case to verify that the form behaves correctly when invalid data is submitted.

    test_rate_limit():
        Test case to verify rate-limiting functionality.
    """

    def setUp(self):
        """
        Setup method to initialize common attributes.

        This method initializes common attributes that are required for the test methods.
        """
        self.url = reverse('contact')

    @override_settings(RATELIMIT_ENABLE=False)
    @patch('contact.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('contact.forms.EmailMessage')
    def test_form_valid(self, _mock_email_message, _mock_captcha_clean):
        """
        Test case to verify that the form correctly redirects upon valid submission
        """
        response = self.client.post(self.url, data=VALID_DATA, HTTP_REFERER='kontakt')
        self.assertRedirects(response, '/kontakt/success/')

    @override_settings(RATELIMIT_ENABLE=False)
    @patch('contact.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('contact.forms.EmailMessage')
    def test_form_invalid(self, _mock_email_message, _mock_captcha_clean):
        """
        Test case to verify that the form behaves correctly when invalid data is submitted.
        """
        for invalid_data in INVALID_DATA_SETS:
            with self.subTest(invalid_data=invalid_data):
                test_data = VALID_DATA.copy()
                test_data.update(invalid_data)
                response = self.client.post(self.url, data=test_data, HTTP_REFERER='kontakt')
                self.assertFalse(response.context['form'].is_valid())
                self.assertEqual(response.status_code, 200)


    @patch('contact.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('contact.forms.EmailMessage')
    def test_rate_limit(self, _mock_email_message, _mock_captcha_clean):
        """
        Test case to verify rate-limiting functionality.
        """
        headers = {
            'HTTP_REFERER': 'kontakt',
            'HTTP_X_REAL_IP': '192.168.1.1',
        }
        limit = int(settings.RATELIMITS[0].split("/")[0])
        for i in range(limit + 1):
            response = self.client.post(self.url, data=VALID_DATA, **headers)
            if i < limit:
                self.assertEqual(response.status_code, 302)
            else:
                self.assertEqual(response.status_code, 429)
