"""
Contact Views Module
=====================

This module contains Django views to manage contact forms. It incorporates rate-limiting
capabilities to prevent misuse.

Functions:
----------
None

Classes:
--------
ContactFormView(FormView)
    Manages the rendering and processing of the contact form, applying rate-limiting when needed.

ContactSuccessView(TemplateView)
    Displays a success message after a successful contact form submission.

ContactRatelimitView(TemplateView)
    Responds with a 429 status code when rate limits are reached, alerting the user.

"""


from django.conf import settings
from django.shortcuts import redirect, reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django_ratelimit.decorators import ratelimit
from cabret.decorator import apply_multiple_decorators
from .forms import ContactForm


class ContactFormView(FormView):
    """
    Manages the rendering and processing of the contact form.

    Attributes:
    -----------
    template_name : str
        The template path used for rendering the form.

    form_class : Type[Form]
        The class responsible for handling form data and validations.

    success_url : str
        The URL to navigate to upon successful form submission.

    ratelimits : list
        Rate-limiting configurations to be applied to the dispatch method.

    Methods:
    --------
    dispatch(*args, **kwargs) -> HttpResponse
        Handles the HTTP methods with applied rate-limiting.

    form_valid(form) -> HttpResponse
        Processes the form when validation succeeds.
    """
    template_name = "contact/index.html"
    form_class = ContactForm
    success_url = "success/"

    ratelimits = [
        ratelimit(key='header:x-real-ip', rate=rate, method='POST', block=True)
        for rate in settings.RATELIMITS
    ]

    @method_decorator(apply_multiple_decorators(ratelimits))
    def dispatch(self, *args, **kwargs):
        """
        Handles HTTP methods and applies rate-limiting.
        """
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        """
        Handles successful form submissions.
        """
        form.send_email()
        return super().form_valid(form)

class ContactSuccessView(TemplateView):
    """
    Presents a success message after successful contact form submission.

    Attributes:
    -----------
    template_name : str
        The template path to show the success message.

    Methods:
    --------
    get(request, *args, **kwargs) -> HttpResponse
        Renders the success message. Redirects if accessed improperly.
    """
    template_name = "contact/success/index.html"

    def get(self, request, *args, **kwargs):
        """
        Renders the success message.
        """
        referer = request.META.get("HTTP_REFERER")
        if referer and "kontakt" in referer:
            return super().get(request, *args, **kwargs)
        return redirect(reverse('contact'))

class ContactRatelimitView(TemplateView):
    """
    Provides feedback to users when rate limits have been exceeded by responding
    with a 429 status code.

    Attributes:
    -----------
    template_name : str
        The template path for the rate-limit exceeded message.

    Methods:
    --------
    get(request, *args, **kwargs) -> HttpResponse
        Renders the rate-limit message with a 429 status, or redirects if accessed improperly.

    post(request, *args, **kwargs) -> HttpResponse
        Handles POST requests that exceed rate limits.
    """
    template_name = "contact/ratelimit/index.html"

    def get(self, request, *args, **kwargs):
        """
        Renders the rate-limit message with a 429 status code.
        """
        referer = request.META.get("HTTP_REFERER")
        if referer and "kontakt" in referer:
            response = super().get(request, *args, **kwargs)
            response.status_code = 429
            return response
        return redirect(reverse('contact'))

    def post(self, request, *args, **kwargs):
        """
        Delegates to the `get` method for handling POST requests
        and responds with a 429 status code.
        """
        response = self.get(request, *args, **kwargs)
        response.status_code = 429
        return response
