"""
URL Patterns for Contact Application
=====================================

This module defines the URL patterns for the Contact application. It uses
Django's built-in `path` function to map URLs to their corresponding views.

Attributes:
-----------
urlpatterns : list
    A list of URL patterns as defined by Django's `path` function. This list is
    processed by Django to route incoming HTTP requests to the appropriate view
    based on the path.

"""

from django.urls import path
from .views import ContactFormView, ContactSuccessView

urlpatterns = [
    path('', ContactFormView.as_view(), name='contact'),
    path('success/', ContactSuccessView.as_view(), name='contact_success')
]
