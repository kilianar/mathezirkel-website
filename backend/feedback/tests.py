"""
Feedback Form Test Module
=========================

This module contains test cases for testing the functionality of the FeedbackForm
and the associated views in Django. It includes tests for valid and invalid form
submissions, as well as rate-limiting functionality.
Note that captcha functionality is not tested.

Classes:
--------
FeedbackFormTest : TestCase
    Class containing test cases for the FeedbackForm.

FeedbackFormViewTest : TestCase
    Class containing test cases for the view that renders the FeedbackForm.

Attributes:
-----------
VALID_DATA : dict
    A dictionary containing a set of valid data for the FeedbackForm.

INVALID_DATA_SETS : list
    A list of dictionaries, each containing a set of invalid data for the FeedbackForm to test.

"""

from unittest.mock import patch
from django.utils.html import escape
from django.urls import reverse
from django.test import TestCase, override_settings
from django.conf import settings
from .forms import FeedbackForm

VALID_DATA = {
    'event_field': '0',
    'message_field': 'This is a test message.',
    'captcha_field': ['mocked_value', 'mocked_value'],
    'privacy_agreement_checkbox': True
}

INVALID_DATA_SETS = [
    {'event_field': ''},
    {'event_field': '1'},
    {'message_field': ''},
    {'message_field': 'x'},
    {'privacy_agreement_checkbox': 'False'},
]

class FeedbackFormTest(TestCase):
    """
    FeedbackForm Test Class

    This class contains test cases to test the functionality of the FeedbackForm.

    Methods:
    --------
    test_form_valid():
        Test case to verify that emails are sent correctly when valid data is submitted.

    test_form_invalid():
        Test case to verify form behavior when invalid data is submitted.

    test_html_escaping():
        Test case to verify that HTML is properly escaped.
    """


    @patch('feedback.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('feedback.forms.EmailMessage')
    def test_form_valid(self, mock_email_message, _mock_captcha_clean):
        """
        Test case to verify that emails are sent correctly when valid data is submitted.
        """
        mock_email_instance = mock_email_message.return_value
        form = FeedbackForm(data=VALID_DATA)
        if not form.is_valid():
            print(form.errors)
        self.assertTrue(form.is_valid())
        form.send_email()
        self.assertTrue(mock_email_instance.send.called)
        _, constructor_kwargs = mock_email_message.call_args
        self.assertEqual(
            constructor_kwargs['subject'],
            f"Anonymes Feedback zu {FeedbackForm.events['0']['name']}"
        )
        self.assertEqual(constructor_kwargs['body'], 'This is a test message.')
        self.assertEqual(constructor_kwargs['to'][0], FeedbackForm.events['0']['mail'][0])

    @patch('feedback.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('feedback.forms.EmailMessage')
    def test_form_invalid(self, mock_email_message, _mock_captcha_clean):
        """
        Test case to verify form behavior when invalid data is submitted.
        """
        for invalid_data in INVALID_DATA_SETS:
            with self.subTest(invalid_data=invalid_data):
                test_data = VALID_DATA.copy()
                test_data.update(invalid_data)
                form = FeedbackForm(data=test_data)
                self.assertFalse(form.is_valid())
                for field in invalid_data:
                    self.assertIn(field, form.errors)
                self.assertFalse(mock_email_message.called)


    @patch('feedback.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('feedback.forms.EmailMessage')
    def test_html_escaping(self, mock_email_message, _mock_captcha_clean):
        """
        Test case to verify that HTML is properly escaped.
        """
        dangerous_data = VALID_DATA.copy()
        dangerous_data['message_field'] = '<script>alert("hack");</script>'
        form = FeedbackForm(data=dangerous_data)
        self.assertTrue(form.is_valid())
        form.send_email()
        _, constructor_kwargs = mock_email_message.call_args
        escaped_message = escape(dangerous_data['message_field'])
        self.assertEqual(constructor_kwargs['body'], escaped_message)

@override_settings(
    CACHES={
        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
            'LOCATION': 'unique-snowflake',
        }
    }
)
class FeedbackFormViewTest(TestCase):
    """
    FeedbackFormView Test Class

    This class contains test cases to test the view that renders the FeedbackForm.

    Attributes:
    -----------
    url : str
        The URL to which the form will be posted.

    Methods:
    --------
    test_form_valid():
        Test case to verify that the form correctly redirects upon valid submission.

    test_form_invalid():
        Test case to verify that the form behaves correctly when invalid data is submitted.

    test_rate_limit():
        Test case to verify rate-limiting functionality.
    """

    def setUp(self):
        """
        Setup method to initialize common attributes.

        This method initializes common attributes that are required for the test methods.
        """
        self.url = reverse('feedback')

    @override_settings(RATELIMIT_ENABLE=False)
    @patch('feedback.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('feedback.forms.EmailMessage')
    def test_form_valid(self, _mock_email_message, _mock_captcha_clean):
        """
        Test case to verify that the form correctly redirects upon valid submission
        """
        response = self.client.post(self.url, data=VALID_DATA, HTTP_REFERER='feedback')
        self.assertRedirects(response, '/feedback/success/')

    @override_settings(RATELIMIT_ENABLE=False)
    @patch('feedback.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('feedback.forms.EmailMessage')
    def test_form_invalid(self, _mock_email_message, _mock_captcha_clean):
        """
        Test case to verify that the form behaves correctly when invalid data is submitted.
        """
        for invalid_data in INVALID_DATA_SETS:
            with self.subTest(invalid_data=invalid_data):
                test_data = VALID_DATA.copy()
                test_data.update(invalid_data)
                response = self.client.post(self.url, data=test_data, HTTP_REFERER='feedback')
                self.assertFalse(response.context['form'].is_valid())
                self.assertEqual(response.status_code, 200)


    @patch('feedback.forms.CaptchaField.clean', return_value='mocked_value')
    @patch('feedback.forms.EmailMessage')
    def test_rate_limit(self, _mock_email_message, _mock_captcha_clean):
        """
        Test case to verify rate-limiting functionality.
        """
        headers = {
            'HTTP_REFERER': 'feedback',
            'HTTP_X_REAL_IP': '192.168.1.1',
        }
        limit = int(settings.RATELIMITS[0].split("/")[0])
        for i in range(limit + 1):
            response = self.client.post(self.url, data=VALID_DATA, **headers)
            if i < limit:
                self.assertEqual(response.status_code, 302)
            else:
                self.assertEqual(response.status_code, 429)
