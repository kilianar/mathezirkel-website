"""
Feedback Views Module
=====================

This module contains Django views for handling feedback forms. It also includes a utility
decorator for applying multiple rate limits to the views.

Functions:
----------
apply_multiple_ratelimits(ratelimit_decorators: list) -> callable
    A decorator function that applies multiple rate-limiting decorators to a view function.

Classes:
--------
FeedbackFormView(FormView)
    Class-based view to handle the rendering and processing of the feedback form.

FeedbackSuccessView(TemplateView)
    Class-based view to display a success message after successful form submission.

FeedbackRatelimitView(TemplateView)
    Class-based view to handle rate-limited requests, responding with a 429 status code.

"""

from functools import wraps
from django.conf import settings
from django.shortcuts import redirect, reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django_ratelimit.decorators import ratelimit
from .forms import FeedbackForm


def apply_multiple_ratelimits(ratelimit_decorators):
    """
    Applies multiple rate-limiting decorators to a view function.

    Parameters:
    -----------
    ratelimit_decorators : list
        A list of rate-limiting decorator functions to apply.

    Returns:
    --------
    callable
        The decorated view function with multiple rate-limiting decorators applied.
    """
    def decorator(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            decorated_func = func
            for dec in reversed(ratelimit_decorators):
                decorated_func = dec(decorated_func)
            return decorated_func(*args, **kwargs)
        return wrapped
    return decorator

class FeedbackFormView(FormView):
    """
    Class-based view to handle the rendering and processing of the feedback form.

    Attributes:
    -----------
    template_name : str
        Path to the template used to render the form.

    form_class : Type[Form]
        Form class used for handling form submissions.

    success_url : str
        URL to redirect to upon successful form submission.

    ratelimits : list
        A list of rate-limiting decorators to apply to the dispatch method.

    Methods:
    --------
    dispatch(*args, **kwargs) -> HttpResponse
        Handles HTTP methods and applies rate-limiting.

    form_valid(form) -> HttpResponse
        Handles successful form submissions.
    """
    template_name = "feedback/index.html"
    form_class = FeedbackForm
    success_url = "success/"

    ratelimits = [
        ratelimit(key='header:x-real-ip', rate=rate, method='POST', block=True)
        for rate in settings.RATELIMITS
    ]

    @method_decorator(apply_multiple_ratelimits(ratelimits))
    def dispatch(self, *args, **kwargs):
        """
        Handles HTTP methods and applies rate-limiting.
        """
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        """
        Handles successful form submissions.
        """
        form.send_email()
        return super().form_valid(form)

class FeedbackSuccessView(TemplateView):
    """
    Class-based view to display a success message after successful form submission.

    Attributes:
    -----------
    template_name : str
        Path to the template used to render the success message.

    Methods:
    --------
    get(request, *args, **kwargs) -> HttpResponse
        Renders the success message.
    """
    template_name = "feedback/success/index.html"

    def get(self, request, *args, **kwargs):
        """
        Renders the success message.
        """
        referer = request.META.get("HTTP_REFERER")
        if referer and "feedback" in referer:
            return super().get(request, *args, **kwargs)
        return redirect(reverse('feedback'))

class FeedbackRatelimitView(TemplateView):
    """
    Class-based view to handle rate-limited requests, responding with a 429 status code.

    Attributes:
    -----------
    template_name : str
        Path to the template used to display the rate-limit message.

    Methods:
    --------
    get(request, *args, **kwargs) -> HttpResponse
        Renders the rate-limit message with a 429 status code.

    post(request, *args, **kwargs) -> HttpResponse
        Delegates to the `get` method for handling POST requests
        and responds with a 429 status code.
    """
    template_name = "feedback/ratelimit/index.html"

    def get(self, request, *args, **kwargs):
        """
        Renders the rate-limit message with a 429 status code.
        """
        referer = request.META.get("HTTP_REFERER")
        if referer and "feedback" in referer:
            response = super().get(request, *args, **kwargs)
            response.status_code = 429
            return response
        return redirect(reverse('feedback'))

    def post(self, request, *args, **kwargs):
        """
        Delegates to the `get` method for handling POST requests
        and responds with a 429 status code.
        """
        response = self.get(request, *args, **kwargs)
        response.status_code = 429
        return response
