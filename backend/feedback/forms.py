"""
Feedback Form Module
====================

This module defines form classes used by the Feedback Django application.

Classes:
--------
FeedbackForm : forms.Form
    A Django form for collecting anonymous feedback.

"""

import json
from django import forms
from django.core.mail import EmailMessage
from django.utils.html import escape
from captcha.fields import CaptchaField

class FeedbackForm(forms.Form):
    """
    Feedback Form Class.

    Defines a form for collecting anonymous feedback related to various events.

    Configuration:
    --------------
    The available events are loaded from a JSON file located at `./feedback/config/conf.json`.

    Attributes:
    -----------
    events: dict
        A dict containing the available events.

    choices : list
        A list of tuple pairs, where the first element is the event key and the
        second is the event name. The list is populated from the JSON file.

    event_field : forms.ChoiceField
        A choice field for selecting the event.

    message_field : forms.CharField
        A text area for inputting the feedback message.

    captcha_field : CaptchaField
        A field for captcha verification.

    privacy_agreement_checkbox : forms.BooleanField
        A checkbox for agreeing to privacy terms.

    Methods:
    --------
    send_email():
        Sends the collected feedback as an email to the respective event's email address.
    """
    with open('./feedback/config/conf.json', 'r', encoding="utf-8") as file:
        events = json.load(file)

    choices = [("","Auswählen")] + [(key, value['name']) for key, value in events.items()]

    event_field = forms.ChoiceField(
        label="Veranstaltung:",
        required=True,
        choices=choices
    )
    message_field = forms.CharField(
        widget=forms.Textarea,
        label="Nachricht:",
        min_length=2,
        max_length=1000,
        required=True
    )
    captcha_field = CaptchaField()
    privacy_agreement_checkbox = forms.BooleanField(required=True)

    def send_email(self):
        """
        Send Feedback as Email.

        Sends the collected feedback as an email to the respective event's email
        address. The email subject contains the event name, and the email body
        contains the feedback message.

        Raises:
        -------
        smtplib.SMTPException: The function will attempt to send the email, and if it cannot, it will raise
        an SMTPException.
        """
        event = self.cleaned_data['event_field']
        message = escape(self.cleaned_data['message_field'])

        subject = f"Anonymes Feedback zu {FeedbackForm.events.get(event).get('name')}"
        recipients = FeedbackForm.events.get(event).get('mail')

        email = EmailMessage(
            subject=subject,
            body=message,
            to=recipients
        )

        email.send(fail_silently=False)
