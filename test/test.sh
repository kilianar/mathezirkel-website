#!/bin/sh

set -e

docker run --rm \
-v $(pwd)/frontend:/src \
--entrypoint "/usr/bin/hugo" \
-u 1000 \
registry.gitlab.com/pages/hugo/hugo_extended:0.118.2

cd test

docker compose up --build
