# Repository License Information

This repository contains a combination of code and content under various licenses.

## Icons

Any icons included in

* [frontend/assets/icons/](frontend/assets/icons/)
* [frontend/themes/mathezirkel-theme/assets/icons/](frontend/themes/mathezirkel-theme/assets/icons/)

are licensed under the [MIT License](https://opensource.org/licenses/MIT).

Each subfolder contains the full text of the MIT License.

## Fonts

Any files included in

* [frontend/themes/mathezirkel-theme/static/fonts/](frontend/themes/mathezirkel-theme/static/fonts/)

are licensed under the [OFL](https://opensource.org/license/ofl-1-1/).

The respective folder holds the precise text of the OFL 1.1 License.

## Other files

Unless explicitly stated, all other files in this repository are licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt), see [LICENSE](./licenses/gpl-3.0.txt).
